const fs = require("fs");
const yaml = require("yaml-js");

const mapJSON = require("./team.json");
const geoTz = require("geo-tz");

geoTz.preCache(); // optionally load all features into memory

const teamYML = yaml.load(fs.readFileSync("./team.yml", "utf8"));

const matchMember = ({ slug }) => {
  return mapJSON.team.find(x => x.slug === slug);
};

function getMemberPicture(picture) {
  if (picture.startsWith("https://")) {
    return picture;
  }
  const pictureName = picture.replace(/\.(png|jpe?g)$/gi, "");
  return `https://about.gitlab.com/images/team/${pictureName}-crop.jpg`;
}

const team = teamYML.map(teamMember => {
  const matchedMember = matchMember(teamMember);

  const picture = getMemberPicture(teamMember.picture);

  if (matchedMember) {
    let timezone;
    if (matchedMember.location) {
      timezone = geoTz(...matchedMember.location)[0];
    }

    return {
      ...teamMember,
      ...matchedMember,
      timezone,
      picture
    };
  }

  return { ...teamMember, picture };
});

fs.writeFileSync(
  "./static/team.json",
  JSON.stringify(
    {
      version: `${mapJSON.version}-with-full-data`,
      team
    },
    null,
    2
  )
);
