export const state = () => ({
  team: [],
  search: "",
  queryString: "",
  loadingState: "LOADING",
  you: null,
  hidden: []
});

function byStartDate(a, b) {
  if (a.start_date === b.start_date) {
    return 0;
  }
  if (typeof a.start_date !== "string") {
    return 1;
  }
  if (typeof b.start_date !== "string") {
    return -1;
  }
  return a.start_date < b.start_date ? -1 : 1;
}

export const actions = {
  loadQueryFromURL({ commit }) {
    const urlParams = new URLSearchParams(window.location.search);

    const search = urlParams.get("search");

    if (search) {
      commit("UPDATE_SEARCH_QUERY", search);
    }
  },
  updateSearch: ({ commit }, message) => {
    commit("UPDATE_SEARCH_QUERY", message);
  },
  async loadTeamData({ commit, state }) {
    if (state.loadingState !== "LOADING") {
      return;
    }
    try {
      const response = await fetch(`./team.json`);
      if (((response.status / 100) | 0) !== 2) {
        throw new Error(
          `Could not get team data: ${response.status} - ${response.statusText}`
        );
      }
      const data = await response.json();

      const team = data.team.map(person => {
        person.role =
          person.role &&
          person.role.replace(/(href=['"])\//gi, "$1https://about.gitlab.com/");
        return person;
      });

      commit("LOADED_TEAM_DATA", team);
    } catch (e) {
      commit("ERROR_LOADING_TEAM_DATA", e);
    }
  }
};

const NEG = "!";
const LEFT_BRACKET = "(";
const RIGHT_BRACKET = ")";

const OPS = new Map()
  .set(NEG, { priority: 5, argLength: 1, fn: a => !a }) // A unary negative symbol
  .set("|", { priority: 2, argLength: 2, fn: (a, b) => a || b })
  .set("&", { priority: 4, argLength: 2, fn: (a, b) => a && b });

const ops = [LEFT_BRACKET, RIGHT_BRACKET, NEG, "|", "&"];

const isOpp = e => ops.includes(e);

const isLeftBracket = e => e === LEFT_BRACKET;
const isRightBracket = e => e === RIGHT_BRACKET;

const noEmptyStrings = e => e !== "";

const isOperator = e => OPS.has(e);

const peek = a => a[a.length - 1];

const opPres = e => (isOperator(e) ? OPS.get(e).priority : 0);

const t1 = (o1, o2) => opPres(o1) <= opPres(o2);

const tokenize = infixString => {
  return infixString
    .split("")
    .reduce(
      ([commandStack, curr], e, i, a) => {
        if (isOpp(e)) {
          commandStack.push(curr);
          commandStack.push(e);
          curr = "";
        } else {
          curr += e;
        }

        if (i === a.length - 1 && curr) {
          commandStack.push(curr);
        }

        return [commandStack, curr, e];
      },
      [[], ""]
    )[0]
    .map(x => x.trim())
    .filter(noEmptyStrings);
};

const toRPN = infixString =>
  tokenize(infixString)
    .reduce(
      ([rS, oS], c) => {
        if (isLeftBracket(c)) {
          oS.push(c);
        } else if (isRightBracket(c)) {
          let nextOpp = oS.pop();
          while (nextOpp !== "(") {
            rS.push(nextOpp);
            nextOpp = oS.pop();
          }
        } else if (!isOperator(c)) {
          rS.push(c);
        } else {
          const o1 = c;
          let o2 = peek(oS);
          let c1 = t1(o1, o2);

          while (o2 && c1) {
            rS.push(oS.pop());
            o2 = peek(oS);
            c1 = t1(o1, o2);
          }
          oS.push(o1);
        }
        return [rS, oS];
      },
      [[], []]
    )
    .reduce((p, c, i) => (i ? [...p, ...c.reverse()] : c), []);

var isPlainObject = function(obj) {
  return Object.prototype.toString.call(obj) === "[object Object]";
};

const isAMatch = (value, search) => {
  if (!value || !search) {
    return false;
  }

  if (Array.isArray(value)) {
    return value.some(x => isAMatch(x, search));
  }

  if (isPlainObject(value)) {
    return isAMatch(Object.values(value), search);
  }

  return value
    .toString()
    .toLowerCase()
    .includes(search.toLowerCase());
};

export const getters = {
  matchCount: state => {
    return state.team.reduce(
      (sum, curr) => {
        if (!state.hidden.includes(curr.slug)) {
          if (curr.type === "person") {
            sum.people += 1;
          } else {
            sum.others += 1;
          }
        }

        return sum;
      },
      { people: 0, others: 0 }
    );
  }
};

const updateURL = ({ search }) => {
  const oldParams = `?${new URLSearchParams(
    window.location.search
  ).toString()}`;

  let newParams = new URLSearchParams(oldParams);

  const updatedParams = { search };

  for (let param in updatedParams) {
    if (updatedParams[param]) {
      newParams.set(param, updatedParams[param]);
    } else {
      newParams.delete(param);
    }
  }

  newParams = "?" + newParams.toString();
  newParams = newParams.length === 1 ? "" : newParams;

  if (oldParams !== newParams) {
    window.history.pushState("", "", `${location.pathname}${newParams}`);
  }

  return newParams;
};

const getHiddenTeamMembers = (team, search) => {
  const searchObject = toRPN(search || "");

  if (!Array.isArray(searchObject) || !searchObject.length) {
    return [];
  }

  const all = [];

  for (let i = 0; i < team.length; i += 1) {
    const person = team[i];

    const stack = [];

    for (let getter of searchObject) {
      if (isOperator(getter)) {
        const operation = OPS.get(getter);
        const args = stack
          .splice(-operation.argLength, operation.argLength)
          .reverse();
        stack.push(operation.fn(...args));
      } else {
        if (getter.includes(":")) {
          const key = getter.split(":")[0];
          const search = getter.split(":")[1];

          const field = person[key];

          stack.push(isAMatch(field, search));
        } else {
          stack.push(isAMatch(person, getter));
        }
      }
    }

    if (!stack[0]) {
      all.push(person.slug);
    }
  }

  return all;
};

export const mutations = {
  UPDATE_SEARCH_QUERY(state, message) {
    if (state.search !== message) {
      state.search = message;
      state.hidden = Object.freeze(getHiddenTeamMembers(state.team, message));
    }
    state.queryString = updateURL(state);
  },
  LOADED_TEAM_DATA(state, team) {
    state.team = Object.freeze(team.sort(byStartDate));
    state.hidden = Object.freeze(
      getHiddenTeamMembers(state.team, state.search)
    );
    state.loadingState = "SUCCESS";
  },
  ERROR_LOADING_TEAM_DATA(state, error) {
    state.loadingState = error;
  },
  updateYourself(state, you) {
    state.you = you;
  }
};
