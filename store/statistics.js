var R = 6371e3; // metres

if (typeof Number.prototype.toRadians === "undefined") {
  Number.prototype.toRadians = function() {
    return (this * Math.PI) / 180;
  };
}

const calculateDistance = ([lat1, lon1], [lat2, lon2]) => {
  var φ1 = lat1.toRadians();
  var φ2 = lat2.toRadians();
  var Δφ = (lat2 - lat1).toRadians();
  var Δλ = (lon2 - lon1).toRadians();

  var a =
    Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
    Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  return R * c;
};

const groupBySorted = (list, field) => {
  const grouped = list.reduce((all, x) => {
    const key = field(x);

    if (!all[key]) {
      all[key] = [];
    }
    all[key].push(x);
    return all;
  }, {});

  return Object.entries(grouped).sort((a, b) => b[1].length - a[1].length);
};

export const getters = {
  filteredTeam: (state, getters, rootState) => {
    return rootState.team.filter(x => !rootState.hidden.includes(x.slug));
  },
  distances: (state, getters) => {
    const distances = {};

    for (let person1 of getters.filteredTeam) {
      for (let person2 of getters.filteredTeam) {
        const key = person1.slug + person2.slug;
        if (person1.slug < person2.slug && !distances[key]) {
          const d = calculateDistance(person1.location, person2.location);
          distances[key] = d;
        }
      }
    }

    return distances;

    return getters.filteredTeam.map(({ name, location }) => {
      const distances = getters.filteredTeam.map(
        ({ name: otherName, location: location2 }) => {
          return { name: otherName, distance: distance(location, location2) };
        }
      );

      return { name, location, distances };
    });
  },
  geographic: (state, getters) =>
    getters.filteredTeam.reduce((all, curr) => {
      if (
        !all.northern ||
        (all.northern.location && all.northern.location[0]) <
          (curr.location && curr.location[0])
      ) {
        all.northern = curr;
      }

      if (
        !all.southern ||
        (all.southern.location && all.southern.location[0]) >
          (curr.location && curr.location[0])
      ) {
        all.southern = curr;
      }

      if (
        !all.eastern ||
        (all.eastern.location && all.eastern.location[1]) <
          (curr.location && curr.location[1])
      ) {
        all.eastern = curr;
      }

      if (
        !all.western ||
        (all.western.location && all.western.location[1]) >
          (curr.location && curr.location[1])
      ) {
        all.western = curr;
      }

      return all;
    }, {}),
  groupedByTimezoneCategory: (state, getters) =>
    groupBySorted(
      getters.filteredTeam,
      x => (x.timezone || "Unknown | Vacancy").split("/")[0]
    ),
  groupedByType: (state, getters) =>
    groupBySorted(getters.filteredTeam, x => x.type),
  groupedByTLD: (state, getters) =>
    groupBySorted(getters.filteredTeam, x => x.departments[0])
};
